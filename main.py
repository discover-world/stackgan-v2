from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import numpy as np
import os.path
import scipy.spatial.distance as sd
from skip_thoughts import configuration
from skip_thoughts import encoder_manager
import pickle

def skip():
    # Set paths to the model.
    VOCAB_FILE = "/home/nc/Desktop/acwork/StackGAN-v2/pretrained/skip_thoughts_bi_2017_02_16/vocab.txt"
    EMBEDDING_MATRIX_FILE = "/home/nc/Desktop/acwork/StackGAN-v2/pretrained/skip_thoughts_bi_2017_02_16/embeddings.npy"
    CHECKPOINT_PATH = "/home/nc/Desktop/acwork/StackGAN-v2/pretrained/skip_thoughts_bi_2017_02_16/model.ckpt-500008"
    # The following directory should contain files rt-polarity.neg and
    # rt-polarity.pos.
    MR_DATA_DIR = "/dir/containing/mr/data"

    # Set up the encoder. Here we are using a single unidirectional model.
    # To use a bidirectional model as well, call load_model() again with
    # configuration.model_config(bidirectional_encoder=True) and paths to the
    # bidirectional model's files. The encoder will use the concatenation of
    # all loaded models.
    encoder = encoder_manager.EncoderManager()
    encoder.load_model(configuration.model_config(bidirectional_encoder=True, encoder_dim=128),
                       vocabulary_file=VOCAB_FILE,
                       embedding_matrix_file=EMBEDDING_MATRIX_FILE,
                       checkpoint_path=CHECKPOINT_PATH)

    encodings = encoder.encode(["test data"])

    print(encodings.shape)
    print(encodings)


def test_load_model():
    with open("/home/nc/Downloads/birds/train/char-CNN-RNN-embeddings.pickle", 'rb') as f:
        embeddings = pickle.load(f, encoding='latin1')
        embeddings = np.array(embeddings)
        # embedding_shape = [embeddings.shape[-1]]
        print('embeddings: ', embeddings.shape)
    return embeddings


if __name__ == '__main__':
    # print(os.environ["PRETRAINED_MODELS_DIR"])
    test_load_model()