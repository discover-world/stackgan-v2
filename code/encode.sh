#/bin/sh

export ROOT="./.."
#export CUDA_VISIBLE_DEVICES=0
export CUB_ENCODER="${ROOT}/models/text_encoder/lm_sje_nc4_cub_hybrid_gru18_a1_c512_0.00070_1_10_trainvalids.txt_iter30000.t7"
# %env CUB_ENCODER=coco_gru18_bs64_cls0.5_ngf128_ndf128_a10_c512_80_net_T.t7
export CAPTION_PATH="${ROOT}/data/test/captions"


if [ -f "$CUB_ENCODER" ]; then
    echo "$CUB_ENCODER exists."
else
    mkdir -p ${ROOT}/models/text_encoder
    pip install gdown
    gdown https://drive.google.com/uc?id=0B0ywwgffWnLLU0F3UHA3NzFTNEE
    mv lm_sje_nc4_cub_hybrid_gru18_a1_c512_0.00070_1_10_trainvalids.txt_iter30000.t7 $CUB_ENCODER
fi

net_txt=${CUB_ENCODER} \
queries=${CAPTION_PATH}.txt \
filenames=${CAPTION_PATH}.t7 \
${TORCH_HOME}/install/bin/th get_embedding.lua