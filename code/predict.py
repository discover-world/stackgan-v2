from miscc.config import cfg_from_file
from miscc.config import cfg
from model import G_NET
import torch.nn as nn
import torch
from torch.autograd import Variable
import torchfile
import os
import numpy as np
from miscc.utils import mkdir_p
from PIL import Image
import subprocess
import sys


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.orthogonal(m.weight.data, 1.0)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)
    elif classname.find('Linear') != -1:
        nn.init.orthogonal(m.weight.data, 1.0)
        if m.bias is not None:
            m.bias.data.fill_(0.0)


def save_singleimages(images, save_dir):
    mkdir_p(save_dir)
    for i in range(images.size(0)):
        fullpath = os.path.join(save_dir, "{0}.png".format(i))
        # range from [-1, 1] to [0, 255]
        img = images[i].add(1).div(2).mul(255).clamp(0, 255).byte()
        ndarr = img.permute(1, 2, 0).data.cpu().numpy()
        im = Image.fromarray(ndarr)
        im.tobytes()
        im.save(fullpath)


cfg_from_file("cfg/eval_birds.yml")
print(cfg.TRAIN)
print('Load ', cfg.TRAIN.NET_G)
net_g = G_NET()
net_g.apply(weights_init)
net_g = torch.nn.DataParallel(net_g, device_ids=[0])
state_dict = torch.load(cfg.TRAIN.NET_G, map_location=lambda storage, loc: storage)
net_g.load_state_dict(state_dict)

nz = cfg.GAN.Z_DIM
noise = Variable(torch.FloatTensor(cfg.TRAIN.BATCH_SIZE, nz))
if cfg.CUDA:
    net_g.cuda()
    noise = noise.cuda()
net_g.eval()


def predict(text):
    data_path = cfg.DATA_DIR
    mkdir_p(data_path)
    text_file = os.path.join(data_path, "captions.txt")
    encode_text_file = os.path.join(data_path, "captions.t7")
    with open(text_file, 'w') as f:
        for i in range(0, 10):
            f.write(text)
            f.write('\n')

    # p = subprocess.Popen(['sh', 'encode.sh'], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    # for line in p.stdout.readlines():
    #     print(line)
    # retval = p.wait()

    os.system("sh encode.sh")

    t_file = torchfile.load(encode_text_file)
    captions_list = t_file.raw_txt
    embeddings = np.concatenate(t_file.fea_txt, axis=0)
    num_embeddings = len(captions_list)
    print('Successfully load sentences from: ', data_path)
    print('Total number of sentences:', num_embeddings)
    print('num_embeddings:', num_embeddings, embeddings.shape)

    embeddings_batch = embeddings
    t_embeddings = Variable(torch.FloatTensor(embeddings_batch))
    if cfg.CUDA:
        t_embeddings = t_embeddings.cuda()

    noise.resize_(embeddings.shape[0], nz)
    noise.data.normal_(0, 1)
    fake_imgs, _, _ = net_g(noise, t_embeddings)

    save_singleimages(fake_imgs[-1], os.path.join(data_path, "output"))


if __name__ == '__main__':
    predict(sys.argv[1])
