from flask_cors import CORS
from flask import Flask, request
from predict import predict
from base64 import encodebytes
from PIL import Image
import io
from flask import jsonify

app = Flask(__name__)
cors = CORS(app, resources={r"*": {"origins": "*"}})


@app.route('/ping')
def success():
    return 'Hello'


@app.route('/predict', methods=['POST', 'GET'])
def _predict():
    data = request.json
    encoded_imgs = []
    predict(data["text"])

    for i in range(10):
        image_path = "../data/test/output/{0}.png".format(i)
        pil_img = Image.open(image_path, mode='r')  # reads the PIL image
        byte_arr = io.BytesIO()
        pil_img.save(byte_arr, format='PNG')  # convert the PIL image to byte array
        encoded_img = encodebytes(byte_arr.getvalue()).decode('ascii')  # encode as base64
        encoded_imgs.append(encoded_img)
    return jsonify(encoded_imgs)
    # return send_file("../data/test/output/0.png", mimetype='image/png')


if __name__ == '__main__':
    app.run(debug=True, port=8080, host='0.0.0.0')
